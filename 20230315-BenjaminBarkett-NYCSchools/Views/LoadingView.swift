//
//  LoadingView.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//
import SwiftUI

struct LoadingView: View {
    var body: some View {
        ZStack {
            Rectangle()
                .ignoresSafeArea()
                .foregroundColor(.white)
                .opacity(0.8)
            ZStack {
                RoundedRectangle(cornerRadius: 15)
                    .background(.clear)
                    .foregroundColor(.gray)
                    .opacity(10)
                ProgressView("Loading")
                    .progressViewStyle(.circular)
                    .tint(.white)
                    .foregroundColor(.white)
            }
            .frame(width: 150, height: 150)
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
