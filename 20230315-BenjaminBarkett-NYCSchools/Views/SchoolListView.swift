//
//  ContentView.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import SwiftUI

struct SchoolListView: View {
    @StateObject var viewModel = SchoolListViewModel()
    
    @State private var isLoading = false
    @State private var showingErrorAlert = false
    
    var body: some View {
        ZStack {
            NavigationView {
                List(viewModel.schoolSearchResults, id: \.dbn) { school in
                    NavigationLink(school.schoolName, destination: SchoolDetailView(dbn: school.dbn))
                }
                .navigationTitle("NYC High Schools")
                .onAppear {
                    viewModel.isLoading = true
                    viewModel.fetchSchools()
                }
                .refreshable {
                    viewModel.isLoading = true
                    viewModel.fetchSchools()
                }
                .searchable(text: $viewModel.searchText)
                .alert("Error Loading Schools", isPresented: $showingErrorAlert) {
                    Button {
                        viewModel.isLoading = true
                        viewModel.fetchSchools()
                    } label: {
                        Text("Retry")
                    }
                    Button(role: .cancel) {
                        viewModel.isLoading = false
                    } label: {
                        Text("Cancel")
                    }
                } message: {
                    Text(viewModel.errorDescription)
                }
            }

            if isLoading {
                LoadingView()
            }
        }
        // These two `.onReceive` modifiers exist because of a behavior in SwiftUI where using the viewModels published values as the binding variables for the loadingView and alert create a runtime warning stating "Publishing changes from within view updates is not allowed, this will cause undefined behaviour"
        // If you were using the `\.dismiss` environment variable, you would also need an `.onChange` modifier to sync the new value of the property back to the viewModel. You could then take both the `.onReceive` and `.onChange` for a property and combine them in a custom ViewModifier.
        .onReceive(viewModel.$isLoading) { isLoading in
            self.isLoading = isLoading
        }
        .onReceive(viewModel.$hasEncounteredError) { hasEncounteredError in
            self.showingErrorAlert = hasEncounteredError
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView()
    }
}
