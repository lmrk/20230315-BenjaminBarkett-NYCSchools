//
//  _0230315_BenjaminBarkett_NYCSchoolsApp.swift
//  20230315-BenjaminBarkett-NYCSchools
//
//  Created by benjamin.barkett on 3/15/23.
//

import SwiftUI

@main
struct _0230315_BenjaminBarkett_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView()
        }
    }
}
