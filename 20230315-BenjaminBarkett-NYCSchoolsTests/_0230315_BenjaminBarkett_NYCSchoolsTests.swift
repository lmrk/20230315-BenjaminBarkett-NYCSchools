//
//  _0230315_BenjaminBarkett_NYCSchoolsTests.swift
//  20230315-BenjaminBarkett-NYCSchoolsTests
//
//  Created by benjamin.barkett on 3/15/23.
//

import XCTest
import Combine
@testable import _0230315_BenjaminBarkett_NYCSchools

final class _0230315_BenjaminBarkett_NYCSchoolsTests: XCTestCase {
    private var session: URLSession!
    private var manager: NetworkManager!
    private var cancellables = Set<AnyCancellable>()

    override func setUp() {
        let config = URLSessionConfiguration.default
        config.protocolClasses = [MockURLProtocol.self]
        session = URLSession(configuration: config)
        manager = NetworkManager(session: session)
    }
    
    func test_invalidStatusCodeThrowsError() {
        let expectation = XCTestExpectation(description: "Fails without publishing value due to invalid status code")
        let error = ["Error": "404: Not found"]
        guard let returnError = try? JSONSerialization.data(withJSONObject: error) else {
            XCTFail("Test error could not be encoded. Test cannot continue.")
            return
        }
        let url = URL(string: "https://wwww.test.com")!
        
        // This is the failure point of the test
        let response = HTTPURLResponse(url: url, statusCode: 404, httpVersion: "HTTP/1.1", headerFields: nil)!
        MockURLProtocol.handler = { _ in
            return (returnError, response)
        }
        
        manager.fetchSchoolList()
            .sink { completion in
                switch completion {
                    case .failure(let error):
                        let err = error as! NetworkingError
                        switch err {
                            case .invalidStatusCode:
                                XCTAssertEqual(err.description, "Invalid status code returned from server")
                                expectation.fulfill()
                            default:
                                XCTFail("Incorrect Error Thrown")
                        }
                    case .finished:
                        break
                }
            } receiveValue: { result in
                XCTFail("This should fail without publishing a value")
            }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 0.5)
    }

}
